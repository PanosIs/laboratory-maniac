from mongoengine import *

class CardInDeck(EmbeddedDocument):
    card_name = StringField(required=True)
    copies = IntField(required=True)

    meta = {
        'db_alias': 'Labman'
    }

class Deck (Document):
    event_name = StringField()
    event_players = IntField()
    event_date = DateField(required=True)

    deck_position = IntField()
    deck_name = StringField()
    deck_type = StringField()

    player_name = StringField(unique_with=['event_name', 'event_date'])

    format = StringField(required=True)
    archetype_name = StringField()

    mainboard =  ListField(EmbeddedDocumentField(CardInDeck), required=True)
    sideboard = ListField(EmbeddedDocumentField(CardInDeck), required=True)

    meta = {

        'db_alias': 'Labman',
        'indexes': [
            'event_date',
            '#deck_type',
            '#archetype_name'
        ]
    }