from src.database.database_model import *
from mongoengine import connect

DATABASE_NAME = 'Labman'

connect(DATABASE_NAME, alias=DATABASE_NAME)

def insert_deck(json):
    try:
        deck = Deck(**json)
        deck.save()
    except:
        pass

def get_decks(skip = 0, limit = 100):
    decks = Deck.objects.only('mainboard').as_pymongo()[skip:skip+limit]
    return decks

