import torch
import torch.nn as nn
import torch.autograd as autograd
import torch.optim as optim
import numpy as np

from src.learning.models.embedding_models import ContinuousBagOfCards
from src.learning.utils.data_preproc import create_data_loader
from src.learning.utils.visualize_embeddings import plot_embeddings

class CBOCTrainer:
    def __init__(self, data_loader, card_pool_size, embedding_size):
        print("Creating model")
        print("Card pool size: " + str(card_pool_size) + " Embedding Size: " + str(embedding_size))
        self.model = ContinuousBagOfCards(card_pool_size, embedding_size)
        self.data_loader = data_loader
        self.card_pool_size, self.embedding_size = card_pool_size, embedding_size
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def train(self, epochs, learning_rate = 0.001):

        print("Transfering model to GPU")
        self.model.to(self.device)

        loss_function = nn.CrossEntropyLoss()
        print("Creating optimizer")
        optimizer = optim.Adam(self.model.parameters(), lr = learning_rate)

        for epoch in range(epochs):

            total_loss = torch.Tensor([0])

            for cards, contexts in self.data_loader:
                contexts = contexts.to(self.device)
                cards = cards.to(self.device)

                contexts = autograd.Variable(contexts)
                cards = autograd.Variable(cards)

                optimizer.zero_grad()
                outputs = self.model.forward(contexts)
                loss = loss_function(outputs, cards)
                loss.backward()
                optimizer.step()

                total_loss += loss.cpu().data

            print('Epoch: %d, Loss: %.3f' % (epoch, total_loss / len(self.data_loader)))

    def save_embeddings(self):
        embeddings = self.model.embeddings.weight.cpu().data.numpy()
        encoder = self.data_loader.dataset.encoder

        card_names = []

        for index in range(len(embeddings)):
            onehot_vector = np.zeros(len(embeddings))
            onehot_vector[index] = 1
            card_names.append(encoder.inverse_card([onehot_vector]))

        with open('embeddings.csv', 'w+') as f:
            for index in range(len(embeddings)):
                f.write(card_names[index] + ',' + str(embeddings[index]) + '\n')

        plot_embeddings(embeddings, card_names)





if __name__ == '__main__':
    print("Pytorch version: " + torch.__version__)
    print("Creating dataset")
    loader = create_data_loader(batch_size=50
                                , deck_limit=500)
    card_pool_size = loader.dataset.card_pool_size
    train = CBOCTrainer(loader, card_pool_size, embedding_size=10)
    train.train(20)
    train.save_embeddings()