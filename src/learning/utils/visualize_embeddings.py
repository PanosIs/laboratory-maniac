import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import numpy as np

def make_plot_color(x, y, names):
    fig = plt.figure(figsize=(16, 12), dpi = 100)
    ax = plt.subplot(111)
    ax.scatter(x, y, c= 'tomato', s=20)

    for index in range(len(names)):
        ax.annotate(names[index], (x[index], y[index]), fontsize=9)
    plt.show()

def plot_embeddings(embeddings, names):
    pca = PCA(n_components=2)
    embeddings_2d = pca.fit_transform(embeddings)
    x, y = embeddings_2d[:, 0], embeddings_2d[:, 1]
    make_plot_color(x, y, names)