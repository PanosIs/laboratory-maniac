from sklearn.preprocessing import OneHotEncoder, LabelEncoder

import src.database.database_api as database_api
import numpy as np
from operator import add, sub
import torch
from torch.utils.data import Dataset, DataLoader

class CardEncoder:
    def __init__(self, card_pool):
        self.onehot_encoder = OneHotEncoder(sparse=False, categories='auto')
        self.onehot_encoder.fit(np.array(card_pool).reshape(-1,1))

    def encode_card(self, card):
        return self.onehot_encoder.transform(np.array([card]).reshape(-1, 1))[0]

    def inverse_card(self, onehot):
        return self.onehot_encoder.inverse_transform(onehot).ravel()[0]

    def encode_deck(self, deck):
        vec = np.zeros(len(self.onehot_encoder.get_feature_names()))
        for card in deck:
            vec = list(map(add, self.encode_card(card), vec))
        return vec

class DeckDataset(Dataset):
    def __init__(self, decks):
            self.card_pool = self._get_card_pool(decks)
            self.card_pool_size = len(self.card_pool)
            self.encoder = CardEncoder(self.card_pool)
            self.data = [self._get_mainboard(deck) for deck in decks]

    def __getitem__(self, index):
        return self.data[index]

    def __len__(self):
        return len(self.data)

    def _get_mainboard(self, deck):
        if (deck.__class__ == dict):
            mainboard = [obj['card_name'] for obj in deck['mainboard']]
        else:
            mainboard = [obj.card_name for obj in deck.mainboard]
        return mainboard

    def _get_card_pool(self, decks):
        card_pool = []
        for deck in decks:
            for card in self._get_mainboard(deck):
                if (card not in card_pool):
                    card_pool.append(card)
        return card_pool

class PairDataset(Dataset):
    def __init__(self, deck_dataset: DeckDataset):
        self.encoder = deck_dataset.encoder
        self.card_pool = deck_dataset.card_pool
        self.card_pool_size = deck_dataset.card_pool_size

        self.cards = []
        self.contexts = []


        for deck in deck_dataset:
            deck_encode = self.encoder.encode_deck(deck)
            for card in deck:
                card_encode = self.encoder.encode_card(card)
                self.cards.append(np.argmax(card_encode))
                deck_encode = list(map(sub, deck_encode, card_encode))
                self.contexts.append(deck_encode)


        self.cards = torch.LongTensor(self.cards)
        self.contexts = torch.LongTensor(self.contexts)

    def __getitem__(self, index):
        return self.cards[index], self.contexts[index]

    def __len__(self):
        return len(self.cards)

def create_data_loader(batch_size = 1, num_workers = 2, shuffle = True, deck_limit = 1):
    deck_dataset = DeckDataset(database_api.get_decks(limit=deck_limit))
    pair_dataset = PairDataset(deck_dataset)
    return DataLoader(pair_dataset, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers)