import torch
import torch.nn as nn
import torch.functional as F
from torch.autograd import Variable

class ContinuousBagOfCards(nn.Module):
    def __init__(self, card_pool_size : int, embedding_dimension : int = 50):
        super(ContinuousBagOfCards, self).__init__()

        self.card_pool_size = card_pool_size
        self.embedding_dimension = embedding_dimension

        self.embeddings = nn.Embedding(card_pool_size, embedding_dimension)
        self.linear = nn.Linear(embedding_dimension, card_pool_size, bias=False)
        self.softmax = nn.Softmax()

    def initialize_embeddings(self):
        in_range = 0.5 / self.embedding_dimension
        self.embeddings.weight.data.uniform_(-in_range, in_range)
        self.linear.weight.data.uniform_(-0,0)
        self.linear.bias.data.zero_()

    def forward(self, input):
        embeds = self.embeddings(input).sum(dim=1)
        out = self.linear(embeds)
        # out = self.softmax(out)
        return out