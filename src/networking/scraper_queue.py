from src.database import database_api
from src.networking.mtgtop8_scraper import *
from concurrent.futures import ThreadPoolExecutor
import time

executor = ThreadPoolExecutor(max_workers=128)

def iterate_decks(links, type, name, format):
    for link in links:
        database_api.insert_deck(scrape_deck(link, deck_type=type, archetype_name=name, format=format))

def iterate_archetypes(link, type, archetype_name, format, page = 1):
    print("Parsing archetype: " + archetype_name + " Page: " + str(page))
    decks, has_next_page = scrape_archetype(link + '&current_page=' + str(page))

    if(has_next_page):
        executor.submit(iterate_archetypes, link, type, archetype_name, format, page=page+1)
    else:
        print("Page end! Archetype: " + archetype_name)

    iterate_decks(decks, type, archetype_name, format)


def iterate_format(format: str):
    aggro, control, combo = scrape_metagame(format)

    futures = []

    for archetype in aggro:
        futures.append(executor.submit(iterate_archetypes, archetype[0], 'aggro', archetype[1], format))

    for archetype in control:
        futures.append(executor.submit(iterate_archetypes, archetype[0], 'control', archetype[1], format))

    for archetype in combo:
        futures.append(executor.submit(iterate_archetypes, archetype[0], 'combo', archetype[1], format))

iterate_format('Modern')
while True:
    time.sleep(60)