import requests
from bs4 import BeautifulSoup
from datetime import datetime

from src.database import database_api

def parse_decklist(deck : str):
    mainboard = []
    sideboard = []
    array = mainboard
    for line in deck.splitlines():
        if(line == 'Sideboard'):
            array = sideboard
        else:
            array.append({'copies' : int(line.split(' ')[0]), 'card_name' : ' '.join(line.split(' ')[1:])})
    return mainboard, sideboard

def scrape_deck(url : str, deck_type : str = "Unknown", archetype_name : str = "Unknown", format : str = "Unknown"):
    page = requests.post(url)

    if page.status_code != 200:
        raise ValueError("Failed to get content.")

    soup = BeautifulSoup(page.content, 'html.parser')

    event_name = soup.find('td', attrs={'class' : 'S18', 'align' : 'center'}).getText()

    event_details = soup.find('td', attrs={'class' : 'S14', 'align' : 'center'}).getText().split("-")

    try:
        event_players = int(event_details[0].split(' ')[1])
    except:
        event_players = None

    try:
        event_date = datetime.strptime(event_details[1].split('\n')[0].replace(' ', ''), '%d/%m/%y').date()
    except:
        try:
            event_date = datetime.strptime(event_details[0].splitlines()[1].split(' ')[1], '%d/%m/%y').date()
        except:
            event_date = None

    deck_details = soup.find('td', attrs={'class' : 'S16', 'width' : '50%'}).getText().split(' ', 1)

    try:
        deck_position = int(deck_details[0][1])
    except:
        deck_position = None

    deck_name, player_name = deck_details[1].split(' - ')

    deck_link = soup.find('div', attrs={'class' : 'Nav_link'}).find('a').get('href')
    deck_file = requests.get('https://www.mtgtop8.com/' + deck_link)

    mainboard, sideboard = parse_decklist(deck_file.content.decode())

    deck_dict = {
        'event_name' : event_name,
        'event_players' : event_players,
        'event_date' : event_date,
        'deck_position' : deck_position,
        'deck_name' : deck_name,
        'archetype_name' : archetype_name,
        'player_name' : player_name,
        'format' : format,
        'mainboard' : mainboard,
        'sideboard' : sideboard,
        'deck_type' : deck_type
    }
    return deck_dict

def scrape_archetype(url : str):
    try:
        page = requests.post(url)

        if page.status_code != 200:
            raise ValueError("Failed to get content.")

        soup = BeautifulSoup(page.content, 'html.parser')

        pages = soup.find_all('div', attrs={'class' : 'Nav_PN'})
        has_next_page = True
        if(len(pages) == 0):
            has_next_page = False
        elif(len(pages) == 1):
            has_next_page = pages[0].getText() == 'Next'

        deck_table = soup.find('table', attrs={'class' : 'Stable', 'width' : "99%"})
        decks = deck_table.find_all('tr', attrs = {"class" : "hover_tr"})

        deck_links = []

        for deck in decks:
            url = 'https://www.mtgtop8.com/' + deck.find('a').get('href')
            deck_links.append(url)
        return deck_links, has_next_page
    except:
        print("Something went the fuck wrong")
        return [], True

def scrape_metagame(format : str):
    if(format == 'Modern'):
        page = requests.post('https://www.mtgtop8.com/format?f=MO&meta=44')
    elif(format == 'Standard'):
        page = requests.post('https://www.mtgtop8.com/format?f=ST&meta=58')
    else:
        return []

    if page.status_code != 200:
        raise ValueError("Failed to get content.")

    soup = BeautifulSoup(page.content, 'html.parser')

    archetype_table = soup.find_all('table', attrs={'class' : 'Stable', 'width' : 330})[0]
    archetypes = archetype_table.find_all('tr')

    aggro_decks = []
    control_decks = []
    combo_decks = []

    count = 0
    for archetype in archetypes:
        if(bool(archetype.find('a'))):
            url = 'https://www.mtgtop8.com/' + archetype.find('a').get('href')
            name = archetype.find('a').getText()
            if(count == 5):
                aggro_decks.append((url, name))
            elif(count == 7):
                control_decks.append((url, name))
            elif(count == 9):
                combo_decks.append((url, name))
        else:
            count += 1

    return aggro_decks, control_decks, combo_decks
